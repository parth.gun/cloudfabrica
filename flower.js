// ==UserScript==
// @name         Final
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       Parth Guntoorkar
// @match        http://www.webpagesthatsuck.com/*
// @grant        none
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @resource     https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/introjs.min.css
// @require      https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/intro.min.js
// @resource     https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css
// @require      https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js
// @require      https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js
// @require      https://www.gstatic.com/firebasejs/5.9.4/firebase-firestore.js

// ==/UserScript==



var fba= document.getElementsByTagName('head')[0];
var fbapp= document.createElement('script');
fbapp.type= 'text/javascript';
fbapp.src= 'https://www.gstatic.com/firebasejs/5.9.4/firebase-app.js';
fba.appendChild(fbapp);


var fbs= document.getElementsByTagName('head')[0];
var fbstore= document.createElement('script');
fbstore.type= 'text/javascript';
fbstore.src= 'https://www.gstatic.com/firebasejs/5.9.4/firebase-firestore.js';
fbs.appendChild(fbstore);



var head= document.getElementsByTagName('head')[0];
var script= document.createElement('script');
script.type= 'text/javascript';
script.src= 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/intro.min.js';
head.appendChild(script);

var bs= document.getElementsByTagName('head')[0];
var bootstrap= document.createElement('script');
bootstrap.type= 'text/javascript';
bootstrap.src= 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js';
bs.appendChild(bootstrap);

var html_doc = document.getElementsByTagName('head')[0];
var css = document.createElement('link');
css.setAttribute('rel', 'stylesheet');
css.setAttribute('type', 'text/css');
css.setAttribute('href', 'https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.7.0/introjs.min.css');
html_doc.appendChild(css);

var bsl = document.getElementsByTagName('head')[0];
var bootlink = document.createElement('link');
bootlink.setAttribute('rel', 'stylesheet');
bootlink.setAttribute('type', 'text/css');
bootlink.setAttribute('href', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
bsl.appendChild(bootlink);

var font = document.getElementsByTagName('head')[0];
var fa = document.createElement('link');
fa.setAttribute('rel', 'stylesheet');
fa.setAttribute('type', 'text/css');
fa.setAttribute('href', 'https://use.fontawesome.com/releases/v5.7.0/css/all.css');
font.appendChild(fa);

var btn=document.createElement("button");
var li=document.createElement("li");
btn.type="submit";
btn.innerHTML = " Help"
btn.onclick = paro;
btn.setAttribute("style", "position: fixed;  left: 22px; bottom: 55px; visibility: visible; border-radius: 12px; background-color: #0088e7")
btn.setAttribute("class", "btn btn-primary far fa-question-circle btn-lg");
document.body.appendChild(btn);

/*var frame = document.createElement("iframe");
frame.setAttribute("id", "frame")
frame.setAttribute("src", "http://localhost/frame.html");
frame.setAttribute("style", "position:fixed; display: none; bottom: 55px; left: 32px; border-Style:solid; border-radius: 12px; border-color: #0088e7");
document.body.appendChild(frame);*/

//Crating main division
var div = document.createElement("div");
div.setAttribute("id", "main")
div.setAttribute("style", "position:fixed; display: none; bottom: 60px; left: 20px; border-Style:solid; border-radius: 12px; border-color: #0088e7");
div.setAttribute("class", "jumbotron ");
document.body.appendChild(div);

//Creating division for heading
var heading = document.createElement("div");
heading.setAttribute("id", "heading")
heading.setAttribute("style","text-align: center")
heading.innerHTML = "<h3>Community Help and Resources<h3><hr>"
document.getElementById("main").appendChild(heading)

//creating Unorder list of tours
var content = document.createElement("div")
content.setAttribute("id", "content")
var ul = document.createElement("ul")



var list1 = document.createElement("li")
var a1 = document.createElement("a")
a1.innerHTML = "<b>  Same page.</b>"
a1.setAttribute("href","#")
a1.onclick = tour1
a1.setAttribute("class","btn  btn-link far fa-comment-alt")
list1.appendChild(a1)

var list2 = document.createElement("li")
var a2 = document.createElement("a")
a2.innerHTML = "<b>  Multipage.</b>"
a2.setAttribute("href","#")
a2.onclick = tour2
a2.setAttribute("class","btn  btn-link far fa-comment-alt")
list2.appendChild(a2)

var list3 = document.createElement("li")
var a3 = document.createElement("a")
a3.innerHTML = " <b> YouTube Video.</b>"
a3.setAttribute("href","#")
a3.onclick = showVideo
a3.setAttribute("class","btn  btn-link far fa-comment-alt")
list3.appendChild(a3)


ul.setAttribute("class", "list-group")
list1.setAttribute("class", "list-group-item ")
list2.setAttribute("class", "list-group-item ")
list3.setAttribute("class", "list-group-item ")

document.getElementById("main").appendChild(content);
document.getElementById("content").appendChild(ul);

var path = document.querySelector("#content ul")
path.appendChild(list1)
path.appendChild(list2)
path.appendChild(list3)



function paro(){

    var temp = document.getElementById("main")
    if (temp.style.display === "none") {
        temp.style.display = "block";
    } else {
        temp.style.display = "none";
    }

}

/*function tour1(){
    var temp = document.getElementById("main")
    temp.style.display = "none";
    var intro = introJs();
    intro.setOptions({
        //showStepNumbers : false,
        //overlayOpacity : 1,
        steps :[

            {
                element : document.getElementById("p7DMMt1_1"),
                //element : "#searchField",
                //element : document.querySelector('#searchField'),
                intro : "We are going ",


            },
            {
                element : document.getElementById("p7DMMt1_7"),
                intro : "Hurray",
                position: "bottom"
            },
            {
                element : document.getElementById("p7DMMt1_4"),
                intro : "Tour Ends",
                position: "top"

            },

        ]
    });
    intro.start().onbeforeexit(function () {
          modal()
        });

}*/



/*****************Tour 1*********************/
let config = {
    apiKey: "AIzaSyDGASROIvxFcrekCANfOOrDOi8yLqcvFY4",
    authDomain: "web-scrapping-chrome-extension.firebaseapp.com",
    databaseURL: "https://web-scrapping-chrome-extension.firebaseio.com",
    projectId: "web-scrapping-chrome-extension",
    storageBucket: "web-scrapping-chrome-extension.appspot.com",
    messagingSenderId: "323166780618"
}

firebase.initializeApp(config);

let db = firebase.firestore();
let steps = [];

db.collection("introData")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            steps.push(doc.data());
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });

function tour1(){
    var temp = document.getElementById("main")
    temp.style.display = "none";
    var intro = introJs();
    intro.setOptions({
        //showStepNumbers : false,
        //overlayOpacity : 1,
        steps : steps.map((step)=>({
            element : document.getElementById(step.element),
            intro : step.intro,
            position: step.position
        }))
    });
    intro.start().onbeforeexit(function () {
          modal()
        });

}


/*****************Tour 2*********************/
function tour2(){
    var temp = document.getElementById("main")
    temp.style.display = "none";
    var intro = introJs();
    intro.setOptions({
        doneLabel : "Go to this page.",
        steps :[

            {
                element : document.getElementById("p7DMMt1_3"),
                intro : "Click on this.",
                position: "top"
            },

        ]
    });
    intro.oncomplete(function(element) {
          window.location.href = 'http://www.webpagesthatsuck.com/bad-web-design.html?multipage=true';
        });
    intro.start()

}

if (RegExp('multipage', 'gi').test(window.location.search)) {

        var intro = introJs();

        intro.setOptions({
            steps: [
                {
                    element : document.getElementById("p7EHCd_1"),
                    intro : "Wow!! We are on second page.",
                    position: "top"
                },

            ]
        });

        intro.start();
    }


/*****************Tour 3*********************/
function showVideo(){
    /*var frame = document.createElement("iframe");
    frame.setAttribute("id", "frame")
    frame.setAttribute("src", "https://www.youtube.com/embed/tgbNymZ7vqY");
    frame.setAttribute("style", "position:fixed; display: none; bottom: 50%; left: 50%; border-Style:solid; border-color: #0088e7");
    document.body.appendChild(frame);

    var temp = document.getElementById("frame")
    if (temp.style.display === "none") {
        temp.style.display = "block";
    }*/
    var temp1 = document.getElementById("main")
    temp1.style.display = "none";

    var myModal1 = document.createElement("div")
    myModal1.setAttribute("id", "myModal1")
    myModal1.setAttribute("class", "modal fade")
    document.body.appendChild(myModal1)
    var lgModal = document.createElement("div")
    lgModal.setAttribute("class", "modal-dialog modal-dialog-centered modal-lg")
    var contentModal = document.createElement("div")
    contentModal.setAttribute("class", "modal-content")
    myModal1.appendChild(lgModal)
    lgModal.appendChild(contentModal)

    var bodyModal = document.createElement("div")
    var frame = document.createElement("iframe");
    frame.setAttribute("id", "frame")
    frame.setAttribute("src", "https://www.youtube.com/embed/kQnNd-DyrpA");
    frame.setAttribute("style", "display: block; width:100%; height:500px");
    bodyModal.setAttribute("class", "modal-body")
    //bodyModal.innerHTML = "Was this tour helpfull ?"
    bodyModal.appendChild(frame)
    contentModal.appendChild(bodyModal)
    $("#myModal1").modal();
}











function modal(){
    var myModal = document.createElement("div")
    myModal.setAttribute("id", "myModal")
    myModal.setAttribute("class", "modal fade")
    document.body.appendChild(myModal)
    var smModal = document.createElement("div")
    smModal.setAttribute("class", "modal-dialog modal-dialog-centered modal-sm")
    var contentModal = document.createElement("div")
    contentModal.setAttribute("class", "modal-content")
    myModal.appendChild(smModal)
    smModal.appendChild(contentModal)


    var headerModal = document.createElement("div")
    var btClose = document.createElement("button")
    btClose.type = "submit"
    btClose.setAttribute("class", "close")
    btClose.setAttribute("data-dismiss", "modal")
    btClose.innerHTML = "&times;"
    headerModal.setAttribute("class", "modal-header")
    headerModal.innerHTML = "<h4 class='modal-title'>Feedback</h4>"
    headerModal.appendChild(btClose)
    contentModal.appendChild(headerModal)


    var bodyModal = document.createElement("div")
    var comment = document.createElement("textarea")
    comment.setAttribute("class", "form-control")
    comment.setAttribute("placeholder", "Please enter your comment if any....")
    bodyModal.setAttribute("class", "modal-body")
    bodyModal.innerHTML = "Was this tour helpfull ?"
    bodyModal.appendChild(comment)
    contentModal.appendChild(bodyModal)

    var footerModal = document.createElement("div")
    footerModal.setAttribute("class", "modal-footer")
    var btYes = document.createElement("button")
    btYes.type = "submit"
    btYes.setAttribute("class", "btn btn-success")
    btYes.innerHTML = "Yes"
    var btNo = document.createElement("button")
    btNo.type = "submit"
    btNo.setAttribute("class", "btn btn-danger")
    btNo.setAttribute("data-dismiss", "modal")
    btNo.innerHTML = "No"
    footerModal.appendChild(btYes)
    footerModal.appendChild(btNo)
    contentModal.appendChild(footerModal)
    $("#myModal").modal();

}

    


